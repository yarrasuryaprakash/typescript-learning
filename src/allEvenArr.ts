export const allEven = (arr: number[]): number[] => {
  const arr2 = [];
  for (let i = 0; i <= arr.length; i++) {
    if (arr[i] % 2 === 0) {
      arr2.push(arr[i]);
    }
  }
  return arr2;
};
