import { concatinate } from './concat';

test('concatinate', () => {
  expect(concatinate([1, 2, 3], [4, 5, 6])).toEqual([1, 2, 3, 4, 5, 6]);
  expect(concatinate([55, 69], [2, 96, 115])).toEqual([55, 69, 2, 96, 115]);
  // expect(concatinate([199,25,83],[49,56])).toEqual([199,25,83,4,56]);
});
