import { allEven } from './allEvenArr';

test('allEven', () => {
  expect(allEven([1, 2, 3, 4, 6, 8])).toEqual([2, 4, 6, 8]);
  //  expect(allEven([86,44,8,95,3])).toEqual([86,44,8,3]);
});
