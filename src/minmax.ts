export const min = (x: number, y: number): number => (x < y ? x : y);
export const max = (x: number, y: number): number => (x > y ? x : y);
