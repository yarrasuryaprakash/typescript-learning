import { range } from './range';

test('range', () => {
  expect(range(2, 9)).toEqual(7);
  // tslint:disable-next-line:no-unused-expression
  expect(range(10, 100)).toEqual(50);
});
console.log(range(2, 100));
