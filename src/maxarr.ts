export const maxarr = (arr9: number[]): number => {
  let m = 0;
  for (const e of arr9) {
    if (m < e) {
      m = e;
    }
  }
  return m;
};
