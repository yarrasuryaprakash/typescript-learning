export const range = (start: number, stop: number): number[] => {
  const arr = [];
  for (let i = start; i < stop; ++i) {
    arr.push(i);
  }
  return arr;
};
