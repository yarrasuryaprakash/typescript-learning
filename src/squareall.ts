export const squareAll = (arr: number[]): number[] => {
  const arr2 = [];
  for (let i = 0; i < arr.length; ++i) {
    arr[i] = arr[i] * arr[i];
    arr2.push(arr[i]);
  }
  return arr2;
};
