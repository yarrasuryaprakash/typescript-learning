export const plus = (x: number, y: number): number => x + y;
export const minus = (x: number, y: number): number => x - y;
