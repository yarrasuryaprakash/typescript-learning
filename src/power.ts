export const power = (x: number, y: number): number => {
  if (y === 0) {
    return 1;
  } else {
    let z = 1;
    for (let i = 1; i <= y; ++i) {
      z = z * x;
    }
    return z;
  }
};
