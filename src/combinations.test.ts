import { factorial, ncr } from './combinations';

test('factorial', () => {
  // expect(factorial(1)).toEqual(1);
  // expect(factorial(4)).toEqual(24);
  expect(factorial(5)).toEqual(120);
});
console.log(factorial(5));

test('ncr', () => {
  // expect(ncr(1, 1)).toEqual(1);
  // expect(ncr(10, 10)).toEqual(1);
  expect(ncr(5, 2)).toEqual(10);
});
console.log(ncr(5, 2));
