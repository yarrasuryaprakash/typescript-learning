export const liesBetween2 = (n: number, x: number, y: number): boolean =>
  n > x && n < y;
