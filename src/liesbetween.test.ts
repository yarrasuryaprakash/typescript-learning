import { liesBetween2 } from './liesbetween';

test('liesBetween2', () => {
  expect(liesBetween2(2, 1, 10)).toEqual(true);
  expect(liesBetween2(10, 5, 100)).toEqual(true);
  expect(liesBetween2(35, 50, 100)).toEqual(false);
});
