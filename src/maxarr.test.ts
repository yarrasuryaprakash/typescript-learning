import { maxarr } from './maxarr';

test('maxarr', () => {
  expect(maxarr([1, 3, 7, 2, 15])).toEqual(15);
  expect(maxarr([100, 33, 17, 992, 715])).toEqual(992);
  // expect(maxarr([65,93,45,9,15])).toEqual(15);
});
