import { power } from './power';

test('power', () => {
  expect(power(2, 0)).toEqual(1);
  expect(power(3, 2)).toEqual(9);
  // expect(power(4,3)).toEqual(16);
});
