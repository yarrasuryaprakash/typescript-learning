// tslint:disable-next-line:ordered-imports
import { min, max } from './minmax';

test('min', () => {
  expect(min(15, 7)).toEqual(7);
  expect(min(155, 999)).toEqual(155);
  // expect(min(772,450)).toEqual(450);
});

test('max', () => {
  expect(max(95, 155)).toEqual(155);
  expect(max(132, 59)).toEqual(132);
  // expect(max(17,40)).toEqual(40);
});
