// console.log(' hello  world');
function min2(x: number, y: number): number {
  if (x < y) {
    return x;
  }
  return y;
}
console.log(min2(1, 2));

function min3(x: number, y: number, z: number): number {
  const m = min2(x, y);
  return min2(m, z);
}
console.log(min3(50, 65, 9));

function max2(x: number, y: number): number {
  if (x > y) {
    return x;
  }
  return y;
}
console.log(max2(100, 500));

function squareAll(arr: number[]): number[] {
  const arr2 = [];
  for (let i = 0; i < arr.length; ++i) {
    arr[i] = arr[i] * arr[i];
    arr2.push(arr[i]);
  }
  return arr2;
}
console.log(squareAll([1, 6, 5]));

function max3(x: number, y: number, z: number): number {
  const m = max2(x, y);
  return max2(m, z);
}
console.log(max3(25, 75, 2));

function isEven(n: number): boolean {
  if (n % 2 === 0) {
    return true;
  }
  return false;
}
console.log(isEven(20));

function isOdd(n: number): boolean {
  if (n % 2 !== 0) {
    return true;
  }
  return false;
}
console.log(isOdd(25));

function liesBetween(n: number, x: number, y: number): boolean {
  if (n > x && n < y) {
    return true;
  }
  return false;
}
console.log(liesBetween(20, 50, 100));

function isFactor(n: number, i: number): boolean {
  if (n % i === 0) {
    return true;
  }
  return false;
}
console.log(isFactor(20, 3));

function isLeap(year: number): boolean {
  if (year % 400 === 0 || (year % 4 === 0 && year % 100 !== 0)) {
    return true;
  }
  return false;
}
console.log(isLeap(2016));

function allEven(arr: number[]): number[] {
  const arr2 = [];
  for (let i = 0; i <= arr.length; i++) {
    if (arr[i] % 2 === 0) {
      arr2.push(arr[i]);
    }
  }
  return arr2;
}
console.log(allEven([1, 2, 4, 6, 9]));

function max(arr: number[]): number {
  let z = 0;
  for (let i = 0; i <= arr.length; ++i) {
    if (z <= arr[i]) {
      z = arr[i];
    }
  }
  return z;
}
console.log(max([0, 5, 105, 16, 2, 153, 333]));

function indexOf(arr: number[], value: number): number {
  for (let i = 0; i <= arr.length; ++i) {
    if (arr[i] === value) {
      return i;
    }
  }
  return -1;
}
console.log(indexOf([1, 2, 5, 9, 6, 4], 15));

function power(x: number, y: number): number {
  if (y === 0) {
    return 1;
  } else {
    let z = 1;
    for (let i = 1; i <= y; ++i) {
      z = z * x;
    }

    return z;
  }
}
console.log(power(2, 4));

function factorial(n: number): number {
  let fact = 1;
  for (let i = 1; i <= n; ++i) {
    fact *= i;
  }
  return fact;
}
console.log(factorial(5));

function ncr(n: number, r: number): number {
  const z = factorial(n) / (factorial(n - r) * factorial(r));

  return z;
}
console.log(ncr(5, 3));

function concatinate(arr1: number[], arr2: number[]): number[] {
  //  return arr1.concat(arr2);
  // for(i=o;i<arr2.length;++i)
  // {
  // arr1.push(arr2[i]);
  // }
  // return arr1;
  for (const e of arr2) {
    console.log(e);
    arr1.push(e);
  }
  return arr1;
}
console.log(concatinate([1, 2, 3, 4], [7, 15, 96, 3]));

const add = (x: number, y: number): number => x + y;
console.log(add(2, 7));

const arr = [1, 2, 3, 4, 5, 6, 7];
const [first, second, ...rest] = arr;
console.log(first, second, rest);

const arr6 = [1, 2, 3, 4, 9];
for (const e of arr6) {
  console.log(e);
}

const min = (x: number, y: number): number => (x < y ? x : y);
console.log(min(5, 7));

const factor = (n: number, i: number): boolean => n % i === 0;
console.log(factor(15, 3));

const even = (n: number): boolean => n % 2 === 0;
console.log(even(26));

const leap = (year: number): boolean =>
  year % 400 === 0 || (year % 4 === 0 && year % 100 !== 0);
console.log(leap(2016));

const liesBetween2 = (n: number, x: number, y: number): boolean =>
  n > x && n < y;
console.log(liesBetween2(5, 10, 100));

const maxarr = (arr9: number[]): number => {
  let m = 0;
  for (const e of arr9) {
    if (m < e) {
      m = e;
    }
  }
  return m;
};
console.log(maxarr([1, 16, 45, 3, 96]));

const indexof = (arr5: number[], value: number): number => {
  for (let i = 0; i < arr5.length; ++i) {
    if (arr6[i] === value) {
      return i;
    }
  }
  return -1;
};
console.log(indexof([1, 9, 46, 109, 5], 109));
