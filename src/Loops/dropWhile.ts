const dropWhile = (
  arr1: ReadonlyArray<number>,
  isEven1: (x: number) => boolean
): ReadonlyArray<number> => {
  for (let i = 0; i < arr1.length; i++) {
    if (!isEven1(arr1[i])) {
      if (isEven1(arr1[i + 1])) {
        return arr1.slice(i);
      }
    }
  }
  return arr1;
};

console.log(dropWhile([2, 8, 4, 13, 6, 18, 27], x => x % 2 === 0));
console.log(dropWhile([3, 2, 8, 13, 6, 18, 27], x => x % 2 === 0));
