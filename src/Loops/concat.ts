const concatinate1 = (
    arr1: ReadonlyArray<number>,
    arr2: ReadonlyArray<number>
  ): ReadonlyArray<number> => {
    for (const e of arr2) {
      arr1 = [...arr1, e];
    }
    return arr1;
  };
  console.log(concatinate1([1, 2, 19, 3, 7], [4, 5, 21, 6]));