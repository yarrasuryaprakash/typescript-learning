const unique = (arr: ReadonlyArray<number>): ReadonlyArray<number> => {
  const result = [];
  let m = 0;
  for (let i = 0; i < arr.length; i++) {
    m++;
    for (let j = i + 1; j < arr.length; j++) {
      if (arr[i] === arr[j]) {
        m++;
      }
    }
    if (m === 1) {
      result.push(arr[i]);
    }
    m = 0;
  }
  return result;
};
console.log(unique([15, 25, 11, 1, 577, 1, 15, 33, 11, 105, 99]));
