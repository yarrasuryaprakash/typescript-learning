const splitAt = (
  arr: ReadonlyArray<number>,
  n: number
): [ReadonlyArray<number>, ReadonlyArray<number>] => {
  const first: number[] = [];
  for (const i = 0; i < n; i + 1) {
    first.push(arr[i]);
  }
  const second: number[] = [];
  for (const i = n; i <= arr.length; i + 1) {
    second.push(arr[i]);
  }
  return [first, second];
};

console.log(splitAt([1, 2, 3, 4, 7, 19, 64, 25], 3));
