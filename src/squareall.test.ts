import { squareAll } from './squareall';

test('squareAll', () => {
  expect(squareAll([1, 2, 3])).toEqual([1, 4, 9]);
  expect(squareAll([3, 6, 9])).toEqual([9, 36, 81]);
  // expect(squareAll([1,2,3])).toEqual([1,4,8]);
});
