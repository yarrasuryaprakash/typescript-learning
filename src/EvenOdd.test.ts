import { isEven, isOdd } from './EvenOdd';

test('isEven', () => {
  expect(isEven(18)).toEqual(true);
  // expect(isEven(9)).toEqual(false);
});

test('isOdd', () => {
  expect(isOdd(1)).toEqual(true);
  // expect(isOdd(8)).toEqual(false);
});
