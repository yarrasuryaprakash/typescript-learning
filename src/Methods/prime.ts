const range = (start: number, stop: number): number[] => {
  const arr2 = [];
  for (let i = start; i <= stop; ++i) {
    arr2.push(i);
  }
  return arr2;
};
console.log(range(1, 5));

const factors = (n: number): number[] => range(1, n).filter(i => n % i === 0);
console.log(factors(24));

const isPrime = (n: number): boolean => factors(n).length === 2;
console.log(isPrime(5));

const primeNumbers = (start: number, stop: number): ReadonlyArray<number> =>
  range(start, stop).filter(n => isPrime(n));
console.log(primeNumbers(1, 40));
