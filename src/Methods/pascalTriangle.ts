const range = (start: number, stop: number): number[] => {
  const arr2 = [];
  for (let i = start; i <= stop; ++i) {
    arr2.push(i);
  }
  return arr2;
};
console.log(range(1, 5));

const factorial1 = (n: number): number =>
  range(1, n).reduce((x, y) => x * y, 1);
console.log(factorial1(4));

const ncr1 = (n: number, r: number) =>
  factorial1(n) / (factorial1(n - r) * factorial1(r));
console.log(ncr1(5, 2));

const pascalLine = (n: number): number[] => range(0, n).map(r => ncr1(n, r));
console.log(pascalLine(4));

const pascalTriangle = (lines: number): ReadonlyArray<ReadonlyArray<number>> =>
  range(0, lines - 1).map(line => pascalLine(line));
console.log(pascalTriangle(5));
