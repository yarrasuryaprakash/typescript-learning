const range = (start: number, stop: number): number[] => {
  const arr2 = [];
  for (let i = start; i <= stop; ++i) {
    arr2.push(i);
  }
  return arr2;
};
console.log(range(1, 5));

const repeat = (x: number, n: number): ReadonlyArray<number> =>
  range(1, n).map(y => (y = x));
console.log(repeat(2, 6));
