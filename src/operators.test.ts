// tslint:disable-next-line:ordered-imports
import { plus, minus } from './operators';

test('plus', () => {
  //  expect(true).toEqual(true);
  expect(plus(9, 2)).toEqual(11);
});
console.log(plus(9, 2));

test('minus', () => {
  expect(minus(9, 2)).toEqual(7);
});
console.log(minus(9, 2));
