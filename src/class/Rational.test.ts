import Rational from './Rational';
test('constructor', () => {
  const r = new Rational(1, 2);
  expect(r.numerator).toEqual(1);
  expect(r.denominator).toEqual(2);
});
test('constructor', () => {
  const r = new Rational(2, 4);
  expect(r.numerator).toEqual(1);
  expect(r.denominator).toEqual(2);
});
test('constructor', () => {
  const r = new Rational(6, 8);
  expect(r.numerator).toEqual(3);
  expect(r.denominator).toEqual(4);
});
test('equals', () => {
  const r = new Rational(1, 2);
  const r2 = new Rational(3, 6);
  const r3 = new Rational(1, 3);
  expect(r.equals(r2)).toBeTruthy();
  expect(r.equals(r3)).toBeFalsy();
});
test('equals', () => {
  const r = new Rational(3, 4);
  const r2 = new Rational(6, 8);
  const r3 = new Rational(1, 3);
  expect(r.equals(r2)).toBeTruthy();
  expect(r.equals(r3)).toBeFalsy();
});
test('add', () => {
  const r = new Rational(1, 2);
  const r2 = new Rational(2, 3);
  const expected = new Rational(7, 6);
  const r3 = r.add(r2);
  expect(r3.equals(expected)).toBeTruthy();
});
test('add', () => {
  const r = new Rational(1, 2);
  const r2 = new Rational(3, 2);
  const expected = new Rational(8, 4);
  const r3 = r.add(r2);
  expect(r3.equals(expected)).toBeTruthy();
});
test('subtract', () => {
  const r = new Rational(1, 2);
  const r2 = new Rational(3, 2);
  const expected = new Rational(-4, 4);
  const r3 = r.subtract(r2);
  expect(r3.equals(expected)).toBeTruthy();
});
test('multiply', () => {
  const r = new Rational(1, 4);
  const r2 = new Rational(2, 3);
  const expected = new Rational(2, 1);
  const r3 = r.multiply(r2);
  expect(r3.equals(expected)).toBeTruthy();
});
test('multiply', () => {
  const r = new Rational(1, 2);
  const r2 = new Rational(6, 1);
  const expected = new Rational(12, 2);
  const r3 = r.multiply(r2);
  expect(r3.equals(expected)).toBeTruthy();
});
test('division', () => {
  const r = new Rational(9, 16);
  const r2 = new Rational(5, 8);
  const expected = new Rational(72, 80);
  const r3 = r.division(r2);
  expect(r3.equals(expected)).toBeTruthy();
});

test('equal', () => {
  const r = new Rational(9, 16);
  const r2 = new Rational(9, 16);
  const r3 = r.equal(r2);
  expect(r3).toBeTruthy();
});
test('compareto', () => {
  const r = new Rational(9, 16);
  const r2 = new Rational(5, 16);
  const r3 = r.equal(r2);
  expect(r3).toBeFalsy();
});
test('compareto', () => {
  const r = new Rational(1, 2);
  const r2 = new Rational(2, 3);
  const r3 = r.greater(r2);
  expect(r3).toBeFalsy();
});
test('compareto', () => {
  const r = new Rational(2, 3);
  const r2 = new Rational(1, 2);
  const r3 = r.greater(r2);
  expect(r3).toBeTruthy();
});
test('compareto', () => {
  const r = new Rational(1, 2);
  const r2 = new Rational(2, 3);
  const r3 = r.less(r2);
  expect(r3).toBeTruthy();
});
