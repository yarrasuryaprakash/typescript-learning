import Complex from './Complex';
test('constructor', () => {
  const r = new Complex(1, 2);
  expect(r.real).toEqual(1);
  expect(r.imaginary).toEqual(2);
});

test('constructor', () => {
  const r = new Complex(6, 8);
  expect(r.real).toEqual(3);
  expect(r.imaginary).toEqual(4);
});
test('equals', () => {
  const r = new Complex(2, 2);
  const r2 = new Complex(2, 2);
  const r3 = new Complex(1, 3);
  expect(r.equals(r2)).toBeTruthy();
  expect(r.equals(r3)).toBeFalsy();
});
test('add', () => {
  const r = new Complex(4, 2);
  const r2 = new Complex(2, 3);
  const expected = new Complex(6, 5);
  const r3 = r.add(r2);
  expect(r3.equals(expected)).toBeTruthy();
});
test('add', () => {
  const r = new Complex(1, 2);
  const r2 = new Complex(3, 2);
  const expected = new Complex(4, 3);
  const r3 = r.add(r2);
  expect(r3.equals(expected)).toBeFalsy();
});
test('subtract', () => {
  const r = new Complex(1, 2);
  const r2 = new Complex(3, 2);
  const expected = new Complex(-2, 0);
  const r3 = r.subtract(r2);
  expect(r3.equals(expected)).toBeTruthy();
});
test('multiply', () => {
  const r = new Complex(3, 4);
  const r2 = new Complex(2, 3);
  const expected = new Complex(-6, 17);
  const r3 = r.multiply(r2);
  expect(r3.equals(expected)).toBeTruthy();
});
test('multiply', () => {
  const r = new Complex(1, 2);
  const r2 = new Complex(6, 1);
  const expected = new Complex(4, 13);
  const r3 = r.multiply(r2);
  expect(r3.equals(expected)).toBeTruthy();
});
test('devide', () => {
  const r = new Complex(2, 3);
  const r2 = new Complex(1, 2);
  const expected = new Complex(1.6, -0.2);
  const r3 = r.devide(r2);
  expect(r3.equals(expected)).toBeTruthy();
});

test('equal', () => {
  const r = new Complex(9, 16);
  const r2 = new Complex(9, 16);
  const r3 = r.equal(r2);
  expect(r3).toBeTruthy();
});
test('compareto', () => {
  const r = new Complex(9, 16);
  const r2 = new Complex(5, 16);
  const r3 = r.equal(r2);
  expect(r3).toBeFalsy();
});
test('compareto', () => {
  const r = new Complex(2, 2);
  const r2 = new Complex(2, 3);
  const r3 = r.greater(r2);
  expect(r3).toBeFalsy();
});
test('compareto', () => {
  const r = new Complex(2, 1);
  const r2 = new Complex(2, 0);
  const r3 = r.greater(r2);
  expect(r3).toBeFalsy();
});
test('compareto', () => {
  const r = new Complex(1, 2);
  const r2 = new Complex(2, 3);
  const r3 = r.less(r2);
  expect(r3).toBeTruthy();
});
